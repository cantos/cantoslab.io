+++
title = "apontamentos [7]"
date = 2024-05-03T15:08:00+01:00
draft = false
+++

1.  "[Exiting the Vampire Castle](https://www.opendemocracy.net/en/opendemocracyuk/exiting-vampire-castle/)", que a Jacobin descreveu como o ensaio do Fisher "most loved and hated".
2.  O drama da [ludificação](https://www.gurwinder.blog/p/why-everything-is-becoming-a-game).
3.  [Keynes](https://mathshistory.st-andrews.ac.uk/Extras/Keynes_Newton/) sobre Newton.
4.  Creonte contra Antígona, na mais antiga referência à anarquia:

    > Não há calamidade maior do que a anarquia. É ela que perde os Estados, que deita por terra as casas, que rompe as filas das lanças aliadas. E àqueles que seguem caminho direito, é a obediência que salva a vida a maior parte das vezes. Deste modo se devem conservar as determinações, e de forma alguma deixá-las  aniquilar por uma mulher. Mais vale, quando é preciso, ser derrubado por um homem, do que sermos apodados de mais fracos que mulheres. (<a href="#citeproc_bib_item_3">Sófocles 1992, 67</a>)
5.  Recomendação do Tyler para [iniciação à música clássica](https://marginalrevolution.com/marginalrevolution/2011/01/classical-music-for-100.html).
6.  [NYker](https://www.newyorker.com/culture/cultural-comment/why-normal-music-reviews-no-longer-make-sense-for-taylor-swift) sobre a Taylor Swift:

    > The tepid music reviews often miss the fact that “music” is something that Swift stopped selling long ago. Instead, she has spent two decades building the foundation of a fan universe, filled with complex, in-sequence narratives that have been contextualized through multiple perspectives across eleven blockbuster installments. She is not creating standalone albums but, rather, a musical franchise.
    >
    > ---
    >
    > Critics, of course, are within their rights to dislike “T.T.P.D.,” and they should be able to do so without fear of dying by a thousand Swiftie-inflicted cuts. But any critique of Swift’s work that doesn’t consider her role as one of the most prominent narrators of our time—and certainly anything that critiques her work as one-dimensional when she’s playing a kind of 4-D chess—will fail to speak to even the most casual of her fans. And without an understanding of the Swiftverse, very little of Swift’s music, or Taylor Swift herself, will ever make any sense.
7.  Ode ao homem:

    > Efémeros! Que somos nós? Que não somos?
    >
    > Sombra de um sonho é o homem!
    >
    > Mas, quando sobrevier um raio de luz divina, um brilhante clarão e doce vida
    >
    > sobrevirá aos homens. Egina, mãe querida, conduz, por caminhos livres,
    >
    > esta cidade, junto com Zeus e com o Éaco soberano,
    >
    > com Peleu e o valente Télamon, e Aquiles também.
    >
    > (<a href="#citeproc_bib_item_2">Píndaro 2003, 85</a>)
8.  [Patrão mal educado, trabalhador mal pago](https://www.publico.pt/2024/04/30/economia/noticia/portugal-pais-patroes-instrucao-5-salario-baixo-2088721).
9.  [Acólito](https://newrepublic.com/article/180487/balaji-srinivasan-network-state-plutocratacolyte) do BAP e do Moldbug.
10. Fim --por agora!-- da odisseia com o Frederico Lourenço como meu mentor:

    > Mas uma coisa ficou certa ao longo destas quase 700 páginas: a leitura da Od. é a mais extraordinária das viagens, em que nos confrontamos com toda a riqueza da experiência humana. Ao mesmo tempo, há o fator não despiciendo do prazer incomparável que o poema oferece a todas as suas leitoras e a todos os seus leitores. O que acontece a cada leitor da Od. quando chega ao fim do poema é algo que, na verdade, as Sereias do Canto 12 anteviram (12.188): «depois de se deleitar, prossegue caminho, mais sabedor». (<a href="#citeproc_bib_item_1">Homero 2018</a>)
11. [Parfit](https://zworld.substack.com/p/could-i-make-you-eat-shit), o profeta.

## ref.

<style>.csl-entry{text-indent: -1.5em; margin-left: 1.5em;}</style><div class="csl-bib-body">
  <div class="csl-entry"><a id="citeproc_bib_item_1"></a>Homero. 2018. <i>Odisseia</i>. Translated by Frederico Lourenço. Lisboa: Quetzal.</div>
  <div class="csl-entry"><a id="citeproc_bib_item_2"></a>Píndaro. 2003. <i>Sete odes de Píndaro</i>. Translated by Maria Helena da Rocha Pereira. Porto: Porto Editora.</div>
  <div class="csl-entry"><a id="citeproc_bib_item_3"></a>Sófocles. 1992. <i>Antígona</i>. Translated by Maria Helena da Rocha Pereira. 3ª ed. Coimbra: Instituto Nacional de Investigação Científica.</div>
</div>
