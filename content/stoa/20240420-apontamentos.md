+++
title = "apontamentos [6]"
date = 2024-04-20T16:54:00+01:00
draft = false
+++

1.  Gozo homérico «ultrajante» contra um Odisseu transformado careca por Atena:

    > Porém Atena não permitiu de modo algum que os arrogantes
    >
    > pretendentes se abstivessem de comportamentos ultrajantes,
    >
    > para que a dor penetrasse ainda mais fundo no coração de Odisseu.
    >
    > Entre eles começou a falar Eurímaco, filho de Pólibo,
    >
    > **[350]** fazendo troça de Odisseu para divertir os companheiros:
    >
    > «Ouvi-me, ó pretendentes da prestigiosa rainha,
    >
    > para que eu diga o que o coração me move a dizer.
    >
    > Não foi à revelia dos deuses que este homem veio ter
    >
    > à casa odisseica: parece que vem dele mesmo a luz das tochas.
    >
    > **[355]** Da careca, é claro, pois ele não tem cabelo!»
    >
    > (<a href="#citeproc_bib_item_1">Homero 2018, chap. 18</a>)
2.  [Autopoiesis](https://en.wikipedia.org/wiki/Autopoiesis):

    > The term autopoiesis (from Greek αὐτo- (auto-) 'self', and ποίησις (poiesis) 'creation, production') refers to a system capable of producing and maintaining itself by creating its own parts. The term was introduced in the 1972 publication Autopoiesis and Cognition: The Realization of the Living by Chilean biologists Humberto Maturana and Francisco Varela to define the self-maintaining chemistry of living cells.
    >
    > ---
    >
    > The philosopher Slavoj Žižek, in his discussion of Hegel, argues:
    >
    > "Hegel is – to use today's terms – the ultimate thinker of autopoiesis, of the process of the emergence of necessary features oHut of chaotic contingency, the thinker of contingency's gradual self-organisation, of the gradual rise of order out of chaos."
    >
    > ---
    >
    > Autopoiesis can be defined as the ratio between the complexity of a system and the complexity of its environment.
    >
    > "This generalized view of autopoiesis considers systems as self-producing not in terms of their physical components, but in terms of its organization, which can be measured in terms of information and complexity. In other words, we can describe autopoietic systems as those producing more of their own complexity than the one produced by their environment." --  Carlos Gershenson, "Requisite Variety, Autopoiesis, and Self-organization"

    {{< figure src="/free/20240420-cell.jpg" link="https://en.wikipedia.org/wiki/Autopoiesis#/media/File:3D-SIM-4_Anaphase_3_color.jpg" >}}
3.  [Tyler](https://marginalrevolution.com/marginalrevolution/2024/04/the-politics-of-civil-war-full-of-spoilers-do-not-read.html) sobre a mensagem do filme Civil War ([2024](https://en.wikipedia.org/wiki/Civil_War_(film))), da A24:

    > The key moment is the scene when they encounter the evil blond-haired guy with the big gun, and he asks “What kind of American are you?”  The naive viewer expects the Socratic dialogue to shift in the direction of red vs. blue states, but no the baddie starts talking about “Central Americans” and “South Americans.”  The real question has become what kinds of Americans are we indeed.
    >
    > The Hong Kong/Chinese guy is shot immediately, once he announces his nationality, again a stand-in for the broader ethnostate divisions the movie is portraying.
    >
    > When the two individuals shift cars, and jump from one moving vehicle to the other, that is the true portend of pending disaster, as Hollis Robbins has pointed out.  Stay in your car (country)!
    >
    > ----
    >
    > Except for the black veteran, the media class are shown as selfish, elevating the scoop above all while disclaiming moral responsibility, enjoying the witnessing of violence, and verging on the psychotic.  It is not an entirely favorable portrait (and yes I do know the director’s words on this).
    >
    > The U.S. citizenry gets rather caught up in fighting the war, and the most positive visions are of the two fathers who retreat to their farms, again a reactionary message.

## ref.

<style>.csl-entry{text-indent: -1.5em; margin-left: 1.5em;}</style><div class="csl-bib-body">
  <div class="csl-entry"><a id="citeproc_bib_item_1"></a>Homero. 2018. <i>Odisseia</i>. Translated by Frederico Lourenço. Lisboa: Quetzal.</div>
</div>
