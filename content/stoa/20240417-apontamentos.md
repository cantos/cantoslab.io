+++
title = "apontamentos [4]"
date = 2024-04-17T15:44:00+01:00
draft = false
+++

1.  O homem que, nas suas palavras, "has mobilized public opinion, launched a grassroots movement, inspired a presidential order, ousted the president of Harvard, recruited a cohort of billionaire defectors, pushed the DEI complex into contraction, and changed the law in every red state, some of which have restricted left-wing race and gender theories in schools, abolished DEI in universities, and instituted universal school choice statewide" debateu --[por escrito](https://im1776.com/2024/04/11/rufo-vs-yarvin/)!-- o Filósofo-Rei da extrema direita.
2.  David Lynch faz [arte mobiliária](https://www.wallpaper.com/design-interiors/david-lynch-salone-del-mobile-2024): [![](/free/20240417-lynch.jpg)](https://www.wallpaper.com/design-interiors/david-lynch-salone-del-mobile-2024)
3.  O Tyler sugere que o NYT está ideolicamente confortável com [abuso infantil](https://marginalrevolution.com/marginalrevolution/2024/04/does-the-nyt-not-recognize-child-abuse.html), e pergunta pela esquerda.
4.  [Prolog](https://en.wikipedia.org/wiki/Prolog):

    > Prolog has its roots in first-order logic, a formal logic, and unlike many other programming languages, Prolog is intended primarily as a declarative programming language: the program is a set of facts and rules, which define relations. A computation is initiated by running a query over the program.[4]
    >
    > Prolog was one of the first logic programming languages[5] and remains the most popular such language today, with several free and commercial implementations available. The language has been used for theorem proving,[6] expert systems,[7] term rewriting,[8] type systems,[9] and automated planning,[10] as well as its original intended field of use, natural language processing.[11][12]
5.  [Prólogo](https://en.wikipedia.org/wiki/Prologue):

    > The Museum of Eterna's Novel by the Argentine writer Macedonio Fernandez has over 50 prologues by the author. Their style varies between metaphysical, humoristic, psychological, discussions about the art of the novel, etc.
6.  O [Macedonio](https://en.wikipedia.org/wiki/The_Museum_of_Eterna%27s_Novel) que:

    > Fernández is widely regarded as a major influence on Jorge Luis Borges, and its writing style bears some resemblance to Borges'. It has been described as an "anti-novel".[1][4] The book is written in a non-linear style, as a set of multi-layered diversions, discursions and self-reflections, with over fifty prologues before the "main" text of the novel begins.[5]
