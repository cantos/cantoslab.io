+++
title = "apontamentos [5]"
date = 2024-04-19T13:05:00+01:00
draft = false
+++

1.  Conselhos do Odisseu:

    > Por isso dir-te-ei uma coisa: presta bem atenção e ouve.
    >
    > **[130]** A terra não alimenta nada de mais frágil que o homem,
    >
    > de tudo quanto na terra respira e rasteja.
    >
    > O homem não pensa vir a sofrer o mal no futuro,
    >
    > enquanto os deuses lhe concedem valor e joelhos resistentes.
    >
    > Mas quando os deuses bem-aventurados lhe dão sofrimentos,
    >
    > **[135]** também isto ele aguenta com tristeza e coração paciente.
    >
    > Pois o estado de espírito dos homens que habitam a terra depende
    >
    > do dia que lhes é trazido pelo pai dos homens e dos deuses.
    >
    > Outrora também eu estava para ser ditoso entre os homens;
    >
    > mas cometi más ações, cedendo à violência e à força,
    >
    > **[140]** confiado no meu pai e nos meus irmãos.
    >
    > Por isso, que nenhum homem seja alguma vez injusto!
    >
    > Que resguarde em silêncio aquilo que os deuses lhe concederem.
    >
    > (<a href="#citeproc_bib_item_1">Homero 2018, chap. 18</a>)
2.  Kruger and Dunning (<a href="#citeproc_bib_item_2">1999, 1</a>), via [Timothy Taylor](https://conversableeconomist.com/2024/04/17/interview-with-david-dunning-of-dunning-kruger-fame/):

    > In 1995, McArthur Wheeler walked into two Pittsburgh banks and robbed them in broad daylight, with no visible attempt at disguise. He was arrested later that night, less than an hour after videotapes of him taken .from surveillance cameras were broadcast on the 11 o’clock news. When police later showed him the surveillance tapes, Mr. Wheeler stared in incredulity. “But I wore the juice,” he mumbled. Apparently, Mr. Wheeler was under the impression that rubbing one’s face with lemon juice rendered it invisible to videotape cameras (Fuocco, 1996).
    >
    > ----
    >
    > We argue that when people are incompetent in the strategies they adopt to
    > achieve success and satisfaction, they suffer a dual burden: Not only do they reach erroneous conclusions and make unfortunate choices, but their incompetence robs them of the ability to realize it. Instead, like Mr. Wheeler, they are left with the mistaken impression that they are doing just fine. … [A]s Charles Darwin (1871) sagely noted over a century ago, “ignorance more frequently begets confidence than does knowledge” (p. 3).
3.  A morte do [Lasch](https://en.wikipedia.org/wiki/Christopher_Lasch):

    > After seemingly successful cancer surgery in 1992, Lasch was diagnosed with metastatic cancer in 1993. Upon learning that it was unlikely to significantly prolong his life, he refused chemotherapy, observing that it would rob him of the energy he needed to continue writing and teaching. To one persistent specialist, he wrote: "I despise the cowardly clinging to life, purely for the sake of life, that seems so deeply ingrained in the American temperament."
4.  [Ne supra crepidam](https://en.wikipedia.org/wiki/Ne_supra_crepidam):

    > Ne supra crepidam ("not beyond the shoe") is a Latin expression used to tell others not to pass judgment beyond their expertise.
    >
    > ----
    >
    > The phrase is recorded in Book 35 of Pliny the Elder's Natural History as ne supra crepidam sutor iudicaret[1] ("Let the cobbler not judge beyond the crepida") and ascribed to the Greek painter Apelles of Kos. Supposedly, Apelles would put new paintings on public display and hide behind them to hear and act on their reception.[2] On one occasion, a shoemaker (sutor) noted that one of the crepida[a] in a painting had the wrong number of straps and was so delighted when he found the error corrected the next day that he started in on criticizing the legs.[2] Indignant, Apelles came from his hiding place and admonished him to keep his opinionating to the shoes.[2] Pliny then states that since that time it had become proverbial.[2]
    >
    > ----
    >
    > Karl Marx ridiculed the idea: "'Ne sutor ultra crepidam' – this nec plus ultra of handicraft wisdom became sheer nonsense, from the moment the watchmaker Watt invented the steam-engine, the barber Arkwright the throstle, and the working-jeweller Fulton the steamship."[7]

## ref.

<style>.csl-entry{text-indent: -1.5em; margin-left: 1.5em;}</style><div class="csl-bib-body">
  <div class="csl-entry"><a id="citeproc_bib_item_1"></a>Homero. 2018. <i>Odisseia</i>. Translated by Frederico Lourenço. Lisboa: Quetzal.</div>
  <div class="csl-entry"><a id="citeproc_bib_item_2"></a>Kruger, Justin, and David Dunning. 1999. “Unskilled and Unaware of It: How Difficulties in Recognizing One’s Own Incompetence Lead to Inflated Self-Assessments.” <i>Journal of Personality and Social Psychology</i> 77 (6): 1121–34. <a href="https://doi.org/10.1037/0022-3514.77.6.1121">https://doi.org/10.1037/0022-3514.77.6.1121</a>.</div>
</div>
