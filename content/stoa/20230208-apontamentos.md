+++
title = "apontamentos [2]"
date = 2023-02-08T22:06:00+00:00
draft = false
+++

1.  A Ilíada é uma música antiquíssima que te permite procurar a humanidade:

    > Only in the songs which tell of these battles does something survive of the splendour and beauty of the art which was destroyed in these protracted wars, for these songs are the Homeric poems, and among the new arrivals were the Greek tribes we know from history. (<a href="#citeproc_bib_item_1">Gombrich 1995, 75</a>)
2.  O [Público](https://www.publico.pt/2023/02/05/politica/noticia/marcelo-trabalha-agenda-improvisada-2036569) desafia os académicos:

    > Nas suas intervenções oficiais, o actual Presidente não divulga os discursos escritos, optando por publicar os vídeos no site, o que pode ser um problema para os académicos que pretendam estudar a presidência de Marcelo.
3.  Os cuidados da [Columbia](https://www.newyorker.com/news/annals-of-education/why-is-columbia-kicking-out-a-beloved-preschool):

    > Why would Columbia allow itself to be seen as the ogre crushing a bunch of little kids and their caregivers under its foot? It may be that, in the decades since the university’s president manifested better child care on campus partly out of a sense of pure chagrin, Columbia has fully metabolized its identity as a giant real-estate-holding company, one with a crucial sideline in educational services that exempts it from paying property taxes.
4.  [Compreensão de escritor](https://newcriterion.com/issues/2023/2/a-genius-at-suffering):

    > Still worse, the women “had nothing in common” with what Vasiliev had expected. They displayed not only the absence of human dignity but a sort of negative dignity, a parody of humanness matching the bordello’s aesthetics of ugliness:
    >
    > “There is vice,” he thought, “but neither consciousness of sin nor hope of salvation. They are bought and sold, steeped in wine and abominations, while they, like sheep, are stupid, indifferent, and don’t understand.” ... It was clear to him, too, that everything that is called human dignity, personal rights, the Divine image and semblance, were defiled to their very foundations.
    >
    > These reflections and his condemnation of the prostitutes’ outlook strike him as cruel, even criminal, and he suffers immense guilt.
5.  [Homens de vermelho](https://www.newyorker.com/magazine/2023/01/30/whats-the-matter-with-men):

    > Reeves offers a wide menu of policies designed to foster a “prosocial masculinity for a postfeminist world.” He would encourage more men to become nurses and teachers, expand paid leave, and create a thousand more vocational high schools. His signature idea, though, is to “redshirt” boys and give them all, by default, an extra year of kindergarten. The aim is to compensate for their slower rates of adolescent brain development, particularly in the prefrontal cortex, which controls decision-making. Reeves, who places great stock in this biological difference, also places great stock in his proposed remedy: “A raft of studies of redshirted boys have shown dramatic reductions in hyperactivity and inattention through the elementary school years, higher levels of life satisfaction, lower chances of being held back a grade later, and higher test scores.”

## ref.

<style>.csl-entry{text-indent: -1.5em; margin-left: 1.5em;}</style><div class="csl-bib-body">
  <div class="csl-entry"><a id="citeproc_bib_item_1"></a>Gombrich, Leonie. 1995. <i>The Story of Art</i>. 16th ed. Phaidon Press.</div>
</div>
