+++
title = "apontamentos [0]"
date = 2023-01-23T22:00:00+00:00
draft = false
+++

1.  Uma traição inevitável:

    > Pouco sabemos acerca desta personagem, sobre quem os testemunhos são contraditórios. Parece, no entanto, que Cilícon era acusado de ter traído a sua cidade, Samos ou Mileto, entregando-a ao inimigo. Quando interrogado sobre o seu acto, limitava-se a responder _πάντʼ ἀγαθά_ [teve de ser]. (<a href="#citeproc_bib_item_1">Aristófanes 1989, 118</a>)
2.  Um jogo maroto:

    > O cótabo era um jogo em voga entre a alta sociedade ateniense. Através dele se procurava detectar os sentimentos da pessoa amada, lançando em taça de metal um pouco de vinho, na esperança de ouvir um som vibrante, testemunho de amor correspondido. (<a href="#citeproc_bib_item_1">Aristófanes 1989, 118</a>)
3.  Dificilmente esquecer-me-ei que Hermes é simultaneamente protetor do comércio e dos ladrões.

## ref.

<style>.csl-entry{text-indent: -1.5em; margin-left: 1.5em;}</style><div class="csl-bib-body">
  <div class="csl-entry"><a id="citeproc_bib_item_1"></a>Aristófanes. 1989. <i>A Paz</i>. Translated by Maria de Fátima de Sousa e Silva. 2nd ed. Instituto Nacional de Investigação Científica.</div>
</div>
