+++
title = "Thiel, com o Tyler"
date = 2024-04-20T16:52:00+01:00
draft = false
+++

1.  Shakespeare e Marx:

    > **COWEN:** You’ve been quoting The Tempest lately in some of your talks. How is it you think the Shakespearean political vision differs from the Christian?
    >
    > **THIEL:** Well, it’s always hard to know what Shakespeare really thought. You certainly have different characters. You have someone like Macbeth who says, “Life is a tale told by an idiot, full of sound and fury, signifying nothing.” That doesn’t sound like a particularly Christian worldview. That’s just what Macbeth says; it’s not what Shakespeare says. It’s always very hard to know, or maybe it’s sort of a Christian nihilistic view of the world or something like that.
    >
    > The contrast I always frame is that the way I understand Shakespeare is always in contrast with someone like Karl Marx, where Marx believed that people had battles over differences that mattered. It was the different classes, and they had objectively different interests, and this is what led to the intensity of the struggle.
    >
    > There’s something in Shakespeare that’s sort of proto-Girardian or very mimetic, where people have conflicts when they . . . The conflicts are the most intense when they don’t differ at all. It’s the opening line of Romeo and Juliet. It’s the Capulets versus Montagues, two houses “alike in dignity.” They’re identical, and that’s why they hate each other so much.
    >
    > Or I think it’s at the end of Hamlet, where Hamlet says to be truly great, you must take everything for an eggshell because an average person would fight over things that mattered, but a truly great person would fight over things as ephemerral as honor or an eggshell or something like this. Of course, Hamlet’s problems — he doesn’t really believe all the insane revenge drama he’s supposed to be in.
    >
    > I think there is probably a place where I would say yes, Shakespeare would probably be very distrustful of extreme ideological differences today. That would probably in some ways also be a kind of political atheist.
    >
    > **COWEN:** I find the play Julius Caesar very interesting because there’s no katechon. There’s no muddling through, so they sacrifice Caesar. There’s a civil war and a lot more people dying and no end to that in sight. It’s the pessimistic scenario of the Thiel mental universe, I think.
    >
    > **THIEL:** There’s a strange way where they’re all going back and thinking they’re reenacting things. The way Brutus gets pulled into the conspiracy in Julius Caesar is that he gets reminded that his ancestor, another person named Brutus, had overthrown Tarquin, the last of the kings of Rome, in 509 BC, so he thinks he’s just reenacting that murder. I think there is some part in the play where Shakespeare has the actor say — I’m going to get this slightly garbled, but it’s something like, “Centuries hence there’ll be people reenacting this on a stage in front of an audience.” This is what motivates Brutus to do it. It’s like the future applause in the Shakespearean theater.
    >
    > Then of course, the crazy literal reenactment of it was John Wilkes Booth shooting Abraham Lincoln in 1865, where Booth was a Shakespearean actor, and then it was “Sic semper tyrannis,” was what he said. He thought he was reenacting the Brutus-Caesar thing.
    >
    > You can look at the 1838 Lincoln speech, the Young Men’s Lyceum address, where Lincoln portrays himself in a somewhat coded way as sort of a proto-Caesar, and he tells the audience there are people in this country who wouldn’t be happy to be . . . Some people are really ambitious, but no one could be like a founder because that was in the past, and the most you can now be is a president. But there are people for whom being president is not enough, and there are some people who, if you didn’t stop them, they would keep going until they enslaved all the white people or freed all the slaves.
    >
    > Lincoln was talking about himself and saying that he has the ambition to be like a Caesar or a Napoleon or something like this. It’s a bit of a roundabout answer. Yes, there are ways we can see it as a cycle, but surely that’s what we want to transcend. It was a bad idea for Brutus to think he was reenacting the Caesar thing, and somehow there was something about the John Wilkes Booth story that’s pretty sad too.
2.  A valorização da matemática é disgénica (à la BAP):

    > **COWEN:** For our last segment, let’s turn to artificial intelligence. As you know, large language models are already quite powerful. They’re only going to get better. In this world to come, will the wordcels just lose their influence? People who write, people who play around with ideas, pundits — are they just toast? What’s this going to look like? Are they going to give up power peacefully? Are they going to go down with the ship? Are they going to set off nuclear bombs?
    >
    > **THIEL:** I’ll say the AI thing broadly, the LLMs — it’s a big breakthrough. It’s very important, and it’s striking to me how bad Silicon Valley is at talking about these sorts of things. The questions are either way too narrow, where it’s something like, is the next transformer model going to improve by 20 percent on the last one or something like this? Or they’re maybe too cosmic, where it’s like from there we go straight to the simulation theory of the universe. Surely there are a lot of in-between questions one could ask. Let me try to answer yours.
    >
    > My intuition would be it’s going to be quite the opposite, where it seems much worse for the math people than the word people. What people have told me is that they think within three to five years, the AI models will be able to solve all the US Math Olympiad problems. That would shift things quite a bit.
    >
    > There’s a longer history I always have on the math versus verbal riff. If you ask, “When did our society bias to testing people more for math ability?” I believe it was during the French Revolution because it was believed that verbal ability ran in families. Math ability was distributed in this idiot savant way throughout the population.
    >
    > If we prioritized math ability, it had this meritocratic but also egalitarian effect on society. Then, I think, by the time you get to the Soviet Union, Soviet Communism in the 20th century, where you give a number theorist or chess grandmaster a medal — which was always a part I was somewhat sympathetic to in the Soviet Union — maybe it’s actually just a control mechanism, where the math people are singularly clueless. They don’t understand anything, but if we put them on a pedestal, and we tell everyone else you need to be like the math person, then it’s actually a way to control. Or the chess grandmaster doesn’t understand anything about the world. That’s a way to really control things.
    >
    > If I fast-forwarded to, let’s say, Silicon Valley in the early 21st century, it’s way too biased toward the math people. I don’t know if it’s a French Revolution thing or a Russian-Straussian, secret-cabal, control thing where you have to prioritize it. That’s the thing that seems deeply unstable, and that’s what I would bet on getting reversed, where it’s like the place where math ability — it’s the thing that’s the test for everything.
    >
    > It’s like if you want to go to medical school, okay, we weed people out through physics and calculus, and I’m not sure that’s really correlated with your dexterity as a neurosurgeon. I don’t really want someone operating on my brain to be doing prime number factorizations in their head while they’re operating on my brain, or something like that.
    >
    > In the late ’80s, early ’90s, I had a chess bias because I was a pretty good chess player. And so my chess bias was, you should just test everyone on chess ability, and that should be the gating factor. Why even do math? Why not just chess? That got undermined by the computers in 1997. Isn’t that what’s going to happen to math? And isn’t that a long-overdue rebalancing of our society?
3.  Crypto e LLMs:

    > **COWEN:** How is manual labor going to do in this world to come? There’ll be a lot more new projects. If you’re a very good gardener, carpenter, will your wages go up by 5X? Or is there something else in store for us?
    >
    > **THIEL:** It’s hard to say, but let me just not give the answer, but let me suggest some of the questions I’d like us to focus on more with AI. I think one question is, how much will it increase GDP versus how much will it increase inequality? Probably it does some of both. Is it a very centralizing technology? That’s another question I’d like to get a better handle on. I had this riff five, six years ago, where if crypto is libertarian, why can’t we say that AI is communist?
4.  O melhor fim:

    > **COWEN:** Final question — what is the next thing you will choose to learn about?
    >
    > **THIEL:** Man, all these questions. This is projections of your personality, Tyler.
    >
    > [laughter]
    >
    > **THIEL**: It’s the Isaiah Berlin thing, where you have the hedgehog who knows one thing and the fox who knows many things. You know so many different things, you’re interested in so many different things. It’s just a few core ideas I come back to, and it’s something like this wonderful and terrible history of the world that we’re living through as Christianity’s unraveling our culture, and we have to figure out a way to get to the other side. I think that’s what’s going to keep me busy for a long time.

[Um dia](https://conversationswithtyler.com/episodes/peter-thiel-political-theology/)...
